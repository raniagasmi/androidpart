package essai.tn;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ProfilMedecin extends AppCompatActivity {
    ImageView img;
    TextView nom, prenom, tel, adresse, specialite, email ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_medecin);
        nom = (TextView)findViewById(R.id.tnom);
        prenom = (TextView)findViewById(R.id.tprenom);
        tel = (TextView)findViewById(R.id.ttel);
        email = (TextView)findViewById(R.id.tmail);
        specialite = (TextView)findViewById(R.id.tspec);
        adresse = (TextView)findViewById(R.id.tadresse);
        img = (ImageView)findViewById(R.id.imgMed);
        //getInfoMedecin();
        getFicheMed();
    }

    private void getFicheMed() {
        class GetDataTask extends AsyncTask<String, Void, JSONObject> {
            JSONParser jsonParser = new JSONParser();
            private ProgressDialog pDialog;
            private String url_apis = "http://192.168.1.111:3000/medecin/afficheParId/" + Accueil.medChoisit;

            @Override
            protected void onPreExecute() {
                pDialog = new ProgressDialog(ProfilMedecin.this);
                pDialog.setMessage("Chargement...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }

            @Override
            protected JSONObject doInBackground(String... params) {

                try {
                    String res = jsonParser.getData(url_apis);
                    JSONObject json_data = new JSONObject(res);
                    if (json_data != null) {
                        System.out.println("json_data     : " + json_data.toString());
                        return json_data;
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject json) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("ressss     : " + json.toString());
                try {
                    nom.setText(json.getString(("nom")));
                    prenom.setText(json.getString(("prenom")));
                    tel.setText(json.getString(("telephone")));
                    adresse.setText(json.getString(("adresseCabinet")));
                    specialite.setText(json.getString(("specialite")));
                    email.setText(json.getString(("email")));
                    String nom_photo = json.getString("image");
                    String url = "E:/pfe/projetdefindetude/controllers/uploads/" + nom_photo;
                    Log.e("url", url);
                    Bitmap bitmap = null;
                    try {
                        bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
                        img.setImageBitmap(bitmap);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        GetDataTask g = new GetDataTask();
        g.execute();

    }
/*
    private void getInfoMedecin() {
        class PostAsync extends AsyncTask<String, String, JSONObject> {
            JSONParser jsonParser = new JSONParser();
            private ProgressDialog pDialog;
            private  String url_apis= "http://192.168.1.16:3000/medecin/afficheParId";


            @Override
            protected void onPreExecute() {
                pDialog = new ProgressDialog(ProfilMedecin.this);
                pDialog.setMessage("Chargement...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }

            @Override
            protected JSONObject doInBackground(String... args) {
                try {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("id", Accueil.medChoisit));
                    String resultt = jsonParser.makeHttpRequest( url_apis, "GET", params);
                    //JSONArray jArray = new JSONArray(resultt);
                    JSONObject json_data = new JSONObject(resultt);
                    if (json_data != null) {
                        System.out.println("json_data     : " + json_data.toString());
                        return json_data;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(JSONObject json) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("json_data AAAA    : " + json);
                if (json != null) {
                    try {
                        // recuperation
                        nom.setText(json.getString(("nom")));
                        prenom.setText(json.getString(("prenom")));
                        tel.setText(json.getString(("tel")));
                        adresse.setText(json.getString(("adresseCabinet")));
                        specialite.setText(json.getString(("specialite")));
                        email.setText(json.getString(("email")));
                        String nom_photo = json.getString("image");
                        String url = "E:/pfe/projetdefindetude/controllers/uploads/"+nom_photo;
                        Log.e("url", url);
                        Bitmap bitmap = null ;
                        try {
                            bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
                            img.setImageBitmap(bitmap);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }

        }

        PostAsync la = new PostAsync();
        la.execute();


    }*/
}

package essai.tn;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Accueil extends AppCompatActivity {
    AutoCompleteTextView esearch;
    static String medChoisit ;//hethi bech n stoki fih l id de medecin choisit static bech nejm ne5oha via dautres class
    ArrayList<Medecin> listMedecins ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        listMedecins = new ArrayList<>();// liste mte3ne vide
        ImageView connexion = (ImageView)findViewById(R.id.imageV); //hethi mte3 l monome de connexion
        esearch = (AutoCompleteTextView)findViewById(R.id.autoTextView);// hethi wen nektbou esm el medecin a recherché
        getAllMedecins();  //lene bech ijibli liste mte3 l medecin lkol wen bech nlwjh al medecin eli 5tartou wy5ou l id mte3ou
        //hethi mte3 licon lid5lt behe ll sign in or sign up
        connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Accueil.this, Connexion.class));
            }
        });

        esearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // fetch the user selected value
                String item = parent.getItemAtPosition(position).toString();
                Medecin country = (Medecin) parent.getItemAtPosition(position);
                medChoisit = country.getId();
                startActivity(new Intent(Accueil.this, ProfilMedecin.class));
            }
        });
    }

    private void getAllMedecins() {
        // asyncTask elle genere automatiquement post pre excute et doinbackground
        class PostAsync extends AsyncTask<String, String, String> {
            //amalna instance JSONParser wen 7atin les methode de consomation des apis
            JSONParser jsonParser = new JSONParser();
            private ProgressDialog pDialog;
            private String URL_PHP = "http://192.168.1.111:3000/medecin/listeMedecins";


            @Override
            protected void onPreExecute() {
            }

            @Override
            protected String doInBackground(String... arg0) {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                //params.add(new BasicNameValuePair("NOM_VARIABLE", VALEUR));
                String jsonStr = jsonParser.makeHttpRequest(URL_PHP,"GET",params);
                System.out.println("Response from url: " + jsonStr);
                try {
                    JSONArray jArray = new JSONArray(jsonStr);
                    for (int i = 0; i< jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        System.out.println("iddd: " + json_data.getString("_id"));
                        //Add countries
                        Medecin m = new Medecin();
                        m.setId(json_data.getString("_id"));
                        m.setName(json_data.getString("nom")+" "+json_data.getString("prenom"));
                        listMedecins.add(m);
                    }
                } catch (final JSONException e) {
                    Log.e("error", "Json parsing error: " + e.getMessage());
                }

                return null;
            }
            protected void onPostExecute(String result) {
               ArrayAdapter<Medecin> adapter = new ArrayAdapter<Medecin>(Accueil.this, android.R.layout.simple_list_item_1, listMedecins);
                esearch.setAdapter(adapter);
            }
        }
        PostAsync p = new PostAsync();
        p.execute();
    }
}

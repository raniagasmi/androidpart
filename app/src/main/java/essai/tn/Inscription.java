package essai.tn;

import androidx.appcompat.app.AppCompatActivity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Inscription extends AppCompatActivity {
    EditText ecin, enom, eprenom, etel, eadr, elog, emdp, eremdp, edate ;
    String nom, prenom, cin, tel, login, password, repassword, date, adresse;
    Button valider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        //ici jai recupéré les valeurs dans ledittext
        ecin = (EditText)findViewById(R.id.ecin);
        enom = (EditText)findViewById(R.id.enom);
        eprenom = (EditText)findViewById(R.id.eprenom);
        etel = (EditText)findViewById(R.id.etel);
        edate = (EditText)findViewById(R.id.enaiss);
        eadr = (EditText)findViewById(R.id.eadresse);
        elog = (EditText)findViewById(R.id.elogin);
        emdp = (EditText)findViewById(R.id.epassword);
        eremdp = (EditText)findViewById(R.id.econf);
         valider = (Button)findViewById(R.id.button);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //je les ai stocké dans des variables de type string
                nom = enom.getText().toString();
                prenom = eprenom.getText().toString();
                login = elog.getText().toString();
                password= emdp.getText().toString();
                date = edate.getText().toString();
                adresse = eadr.getText().toString();
                tel = etel.getText().toString();
                cin = ecin.getText().toString();
                repassword = eremdp.getText().toString();
                // noublier pas ici de verifier si les champs ont bien remplis ou nn
if(nom.equals("")||prenom.equals("")||login.equals("")||password.equals("")||date.equals("")||adresse.equals("")||tel.equals("")||cin.equals("")||repassword.equals("")){
    Toast.makeText(Inscription.this, "Veuillez remplir tout les champs svp", Toast.LENGTH_LONG).show();

}
 else if(!password.equals(repassword)){
    Toast.makeText(Inscription.this, "Veuillez confirmer votre mot de passe", Toast.LENGTH_LONG).show();

}
          else{
              addUser();
    startActivity(new Intent(Inscription.this, Connexion.class));
}
            }
        });
    }

    private void addUser() {
        class PostAsync extends AsyncTask<String, String, String> {
            JSONParser jsonParser = new JSONParser();
            private ProgressDialog pDialog;
            // ici attention localhost ou bien 127.0.0.1 sa marche pas il faut mettre vitre adresse ip ou bien 10.0.2.2
            //  "http://10.0.3.2/isg_android/profil.php";
            private  String url_api = "http://192.168.1.111:3000/patient/ajouter";


            @Override
            protected void onPreExecute() {
                pDialog = new ProgressDialog(Inscription.this);
                pDialog.setMessage("Inscription en cours...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }


            @Override
            protected String doInBackground(String... args) {
                String resultt = null ;
                try {
                    JSONObject dataToSend = new JSONObject();
                    dataToSend.put("nom", nom);
                    dataToSend.put("prenom", prenom);
                    dataToSend.put("telephone", tel);
                    dataToSend.put("login", login);
                    dataToSend.put("password", password);
                    dataToSend.put("dateNaissance", date);
                    dataToSend.put("adresse", adresse);
                    dataToSend.put("cin", cin);
                    resultt = jsonParser.postData(url_api, dataToSend);
                    Log.d("resultt result --->", resultt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return resultt;
            }

            protected void onPostExecute(String res) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("res api    : " + res);
                if (res.contains(("OK"))) {
                    Toast.makeText(Inscription.this, "Inscription effectuée avec succes", Toast.LENGTH_SHORT).show();
                }
              /*  else{
                    Toast.makeText(Inscription.this, "Erreur Inscription", Toast.LENGTH_SHORT).show();
                }
*/

            }

        }

        PostAsync la = new PostAsync();
        la.execute();
    }

}

package essai.tn;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ListMedecins extends AppCompatActivity {
    ListView listM ;
    SearchView searchView;
    List<HashMap<String, Object>> medecinLIST;
    SimpleAdapter adapter1 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_medecins);
        //listM = (ListView)findViewById(R.id.listM);
        searchView=(SearchView) findViewById(R.id.searchView);
        searchView.setQueryHint("Search");
        medecinLIST = new ArrayList<>();
        getAllMedecins();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String text = newText;
                adapter1.getFilter().filter(text);
                return false;
            }
        });
    }

    private void getAllMedecins() {
        class PostAsync extends AsyncTask<String, String, String> {
            JSONParser jsonParser = new JSONParser();
            private ProgressDialog pDialog;
            private String URL_PHP = "http://192.168.1.111:3000/medecin/listeMedecins";


            @Override
            protected void onPreExecute() {
                pDialog = new ProgressDialog(ListMedecins.this);
                pDialog.setMessage("Loading ...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }

            @Override
            protected String doInBackground(String... arg0) {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                //params.add(new BasicNameValuePair("NOM_VARIABLE", VALEUR));
                String jsonStr = jsonParser.makeHttpRequest(URL_PHP, "GET", params);
                System.out.println("Response from url: " + jsonStr);
                try {
                    JSONArray jArray = new JSONArray(jsonStr);
                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject json_data = jArray.getJSONObject(i);
                        HashMap<String, Object> mapp = new HashMap<>();
                        mapp.put("NP", json_data.getString("nom")+" "+json_data.getString("prenom"));
                        mapp.put("ADR", json_data.getString("adresseCabinet"));
                        medecinLIST.add(mapp);
                    }
                }
                catch (final JSONException e) {
                    Log.e("error", "Json parsing error: " + e.getMessage());
                }
                return null;
            }

            protected void onPostExecute(String result) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                  adapter1 = new SimpleAdapter(ListMedecins.this, medecinLIST,
                        R.layout.item_medecin, new String[]{"NP", "ADR"},
                        new int[]{R.id.tnp, R.id.tadresse});
                listM.setAdapter(adapter1);
            }
        }

        PostAsync p = new PostAsync();
        p.execute();
    }
}

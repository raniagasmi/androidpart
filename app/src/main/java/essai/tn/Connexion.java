package essai.tn;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Connexion extends AppCompatActivity {
    EditText id,mdp;
    TextView cnx,inscri;
    String identifiant,mot_de_passe;
    static String idUSer, user ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        //declaration
         id=(EditText)findViewById(R.id.edlog);
         mdp=(EditText)findViewById(R.id.edpw);
         cnx=(TextView)findViewById(R.id.txtcnx);
         inscri=(TextView)findViewById(R.id.txtinscri);

        //action sur textView s'inscrire
        inscri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Connexion.this, Inscription.class));
            }
        });

        //action sur textView se connecter
        cnx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //traitement à faire lors de cliquer sur se connecter
                //recuperation de contenu des champs de texte
                identifiant = id.getText().toString();
                mot_de_passe = mdp.getText().toString();
                //verification si login ou passeword sont vides
                if (identifiant.equals("")) { 
                    id.setError("SVP saisir votre identifiant!");
                    return;
                }
                if(mot_de_passe.equals("")){
                    mdp.setError("SVP saisir votre mot de passe!");
                    return;
                }
                //verification si login et password sont correctes

                verifPatient();
            }
        });
    }

    private void verifPatient() {
        class PostAsync extends AsyncTask<String, String, JSONObject> {
            JSONParser jsonParser = new JSONParser();
            private ProgressDialog pDialog;
            // ici attention localhost ou bien 127.0.0.1 sa marche pas il faut mettre vitre adresse ip ou bien 10.0.2.2
            //  "http://10.0.3.2/isg_android/profil.php";
            private  String url_php = "http://192.168.1.111:3000/utilisateur/authentification";


            @Override
            protected void onPreExecute() {
                pDialog = new ProgressDialog(Connexion.this);
                pDialog.setMessage("Connexion en cours...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }


            @Override
            protected JSONObject doInBackground(String... args) {
                //String resultt = null ;
                try {
                    JSONObject dataToSend = new JSONObject();
                    dataToSend.put("login", identifiant);
                    dataToSend.put("password", mot_de_passe);
                    //dataToSend.put("ACTION", "INSERT");
                    String resultt = jsonParser.postData(url_php, dataToSend);
                    Log.d("resultt result --->", resultt);
                    JSONObject test = new JSONObject(resultt).getJSONObject("data");
                    JSONObject json_data = test.getJSONObject("user");
                    if (json_data != null) {
                        System.out.println("json_data     : " + json_data.toString());
                        return json_data;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(JSONObject res) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("res api    : " + res);
                if(res!=null) {
                    try {
                        String nom = res.getString("nom");
                        System.out.println("nom is   : " + nom);
                        startActivity(new Intent(Connexion.this, MenuPrincipal.class));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(Connexion.this, "SVP verifier votre login et password", Toast.LENGTH_SHORT).show();
                }




            }

        }

        PostAsync la = new PostAsync();
        la.execute();
    }
/*
    private void verifPatient() {
        class PostAsync extends AsyncTask<String, String, JSONObject> {
            JSONParser jsonParser = new JSONParser();
            private ProgressDialog pDialog;
            // ici attention localhost ou bien 127.0.0.1 sa marche pas il fait ùettre votre adresse ip ou bien 10.0.2.2
            private  String url_php = "http://192.168.1.12/webservices/authentification.php";


            @Override
            protected void onPreExecute() {
                pDialog = new ProgressDialog(Connexion.this);
                pDialog.setMessage("Connexion...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }

            @Override
            protected JSONObject doInBackground(String... args) {
                try {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("ID", "1"));
                    String resultt = jsonParser.makeHttpRequest( url_php, "POST", params);
                    JSONArray jArray = new JSONArray(resultt);
                    JSONObject json_data = jArray.getJSONObject(0);
                    if (json_data != null) {
                        Log.d("JSON result", jArray.toString());
                        System.out.println("json_data     : " + json_data.toString());
                        return json_data;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(JSONObject json) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("json_data AAAA    : " + json);
                if (json != null) {
                    try {
                        // recuperation
                        user = json.getString("nom")+" "+json.getString("prenom");
                        idUSer = json.getString("id");
                        Intent i = new Intent(getApplicationContext(), MenuPrincipal.class);
                        startActivity(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(Connexion.this, "SVP verifier votre login et mot de passe", Toast.LENGTH_SHORT).show();
                }


            }

        }

        PostAsync la = new PostAsync();
        la.execute();


    }*/
}
